$TITLE PROBLEM 2

SETS
K "Keys"        /k1*k30/
i "Nodes"       /n1*n8/
;
ALIAS(i,j);

PARAMETERS
mk  "Each key k occupies a space of m(k)"
Mi  "Keys associated to a given node i must not exceed the memory limit Mi of the node"
q   "Two nodes communicate directly if they share at least q keys"
T   "Each key must be used at most T times"
;

mk  = 186;
Mi  = 1000;
q   = 3;
T   = 2;

BINARY VARIABLES
x(k,i)      "1 if node i has key k, 0 otherwise"
y(i,j,k)    "1 if node i and j has key k, 0 otherwise. i different from j."
delta(i,j)  "1 if node i and j are directly connected, 0 otherwise. i different from j."
;

VARIABLES
z           "Value of objective function"
;

EQUATIONS
Obj         "The objective function"
Con1(i)     "Constraint 1"
Con2(k)     "Constraint 2"
Con3(i,j)   "Constraint 3"
Con4(i,j,k) "Constraint 4"
Con5(i,j,k) "Constraint 5"
;

*We want to maximize the number of direct connections
Obj..           z =E= sum(i,sum(j$(ord(i) <> ord(j) and ord(i)>ord(j)), delta(i,j)));

*Keys associated to a given node i must not exceed the memory limit Mi of the node
Con1(i)..       sum(k, mk*x(k,i)) =L= Mi;

*Each key must be used at most T times
Con2(k)..       sum(i, x(k,i)) =L= T;

*Definition of delta(i,j), forcing it to be 1 when node i and j are directly connected, and 0 otherwise.
Con3(i,j)..     sum(k, y(i,j,k)) =G= q*delta(i,j);

*Definition of y(i,j,k), forcing it to be 1 when node i and j has key k, and 0 otherwise.
Con4(i,j,k)..   x(k,i)+x(k,j)$(ord(i) <> ord(j) and ord(i)>ord(j)) =G= 2*y(i,j,k);
Con5(i,j,k)..   x(k,i)+x(k,j)$(ord(i) <> ord(j) and ord(i)>ord(j)) =L= y(i,j,k)+1;

MODEL model2 /all/;

SOLVE model2 USING mip MAXIMIZING z;

DISPLAY z.l, x.l, y.l, delta.l;