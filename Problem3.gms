$TITLE PROBLEM 3
$eolcom //

SETS
V "Ships" /v1*v5/
R "Routes" /Asia, ChinaPacific/
P "Ports" /Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/
H1(P) "First pair of incompatible ports" /Singapore,Osaka/
H2(P) "Second pair of incompatible ports" /Incheon, Victoria/
;

PARAMETER
F(V) "Fixed cost if ship V is used"
    /v1 65
     v2 60
     v3 92
     v4 100
     v5 110/

G(V) "Amount of days ship V can sail in a year"
    /v1 300
     v2 250
     v3 350
     v4 330
     v5 300/

D(P) "Amount of years company at least has to visit port P, if they decide to visit port P"
    /Singapore  15
     Incheon    18
     Shanghai   32
     Sydney     32
     Gladstone  45
     Dalian     32
     Osaka      15
     Victoria   18/
;

TABLE C(V,R) "Cost for ship V to ocmplete route r"
            Asia    ChinaPacific
    v1      1.41    1.9
    v2      3.0     1.5
    v3      0.4     0.8
    v4      0.5     0.7
    v5      0.7     0.8
;

TABLE T(V,R) "Cost for ship V to ocmplete route r"
            Asia    ChinaPacific
    v1      14.4    21.2
    v2      13.0    20.7
    v3      14.4    20.6
    v4      13.0    19.2
    v5      12.0    20.1
;


TABLE A(P,R) "1 if route R passes through port P, 0 otherwise"
                Asia    ChinaPacific
    Singapore   1       0
    Incheon     1       0
    Shanghai    1       1
    Sydney      0       1
    Gladstone   0       1
    Dalian      1       1
    Osaka       1       0
    Victoria    0       1
;

SCALAR
K /5/;

VARIABLES
z       "Value of objective function"
;

BINARY VARIABLES
x(V)    "1 if ship V is used, 0 otherwise"
y(P)    "1 if port p is visited, 0 otherwise"
;

INTEGER VARIABLES
q(V,R)  "Number of times ship V sails route R"
;

EQUATIONS
Obj         "The objective function"
Con1        "Constraint 1"
Con2(P)     "Constraint 2"
Con3        "Constraint 3"
Con4        "Constraint 4"
Con5(V)     "Constraint 5"
;

*Our objective is to minimize the total yearly cost
Obj..       z =E= sum(V, F(V)*x(V))+sum(V, sum(R, C(V,R)*q(V,R)));

*SuperSea has to service at least K of a set P of ports
Con1..      sum(P, y(p)) =G= K;

*If the company decided to visit port P, they would have to visit the port at least D(P) times in a year
Con2(P)..   sum(R, sum(V, A(P,R)*q(V,R))) =G= y(P)*D(P);

*Definition of first set of incompatible ports
Con3..      sum(H1(P), y(P)) =L= 1;    

*Definition of second set of incompatible ports
Con4..      sum(H2(P), y(P)) =L= 1; 

*Ship V can sail no more than G(V) days in a year
Con5(V)..   sum(R, T(V,R)*q(V,R)) =L= G(V)*x(V);

MODEL model3 /all/;

SOLVE model3 USING mip MINIMIZING z;

DISPLAY z.l, x.l, y.l, q.l;






