$TITLE PROBLEM 1

SETS
F       "Formation"         /442,352,4312,433,343,4321/
R       "Role"              /GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW/
P       "Player"            /P1*P25/
Q(P)    "Quality players"   /P13,P20,P21,P22/
S(P)    "Strength players"  /P10,P12,P23/
;

TABLE A(F,R) "Table 1: Number of players required for each formation and role"
        GK  CDF LB  RB  CMF LW  RW  OMF CFW SFW
442     1   2   1   1   2   1   1   0   2   0
352     1   3   0   0   3   1   1   0   1   1   
4312    1   2   1   1   3   0   0   1   2   0
433     1   2   1   1   3   0   0   0   1   2
343     1   3   1   1   3   0   0   2   1   0
4321    1   2   1   1   3   0   0   2   1   0
;

TABLE B(P,R) "Table 2: Fitness player-role"
        GK  CDF LB  RB  CMF LW  RW  OMF CFW SFW
P1      10  0   0   0   0   0   0   0   0   0
P2      9   0   0   0   0   0   0   0   0   0
P3      8.5 0   0   0   0   0   0   0   0   0   
P4      0   8   6   5   4   2   2   0   0   0
P5      0   9   7   3   2   0   2   0   0   0
P6      0   8   7   7   3   2   2   0   0   0
P7      0   6   8   8   0   6   6   0   0   0
P8      0   4   5   9   0   6   6   0   0   0
P9      0   5   9   4   0   7   2   0   0   0
P10     0   4   2   2   9   2   2   0   0   0
P11     0   3   1   1   8   1   1   4   0   0
P12     0   3   0   2   10  1   1   0   0   0
P13     0   0   0   0   7   0   0   10  6   0
P14     0   0   0   0   4   8   6   5   0   0
P15     0   0   0   0   4   6   9   6   0   0
P16     0   0   0   0   0   7   3   0   0   0
P17     0   0   0   0   3   0   9   0   0   0 
P18     0   0   0   0   0   0   0   6   9   6
P19     0   0   0   0   0   0   0   5   8   7
P20     0   0   0   0   0   0   0   4   4   10
P21     0   0   0   0   0   0   0   3   9   9
P22     0   0   0   0   0   0   0   0   8   8
P23     0   3   1   1   8   4   3   5   0   0
P24     0   3   2   4   7   6   5   6   4   0
P25     0   4   2   2   6   7   5   2   2   0
;

VARIABLES
z       "Value of objective function"

BINARY VARIABLES
x(P,R)  "Decision variable. 1 if player P and role R is chosen, 0 otherwise"
y(F)    "Binary variable: 1 if formation F is chosen, 0 otherwise"
;

EQUATIONS
Obj      "The objective function"
Con1     "Constraint 1"
Con2(R)  "Constraint 2"
Con3(P)  "Constraint 3"
Con4     "Constraint 4"
Con5     "Constraint 5"
;

*Our objective is to maximize total fitness player-role
Obj..       z =E= sum(P,sum(R,B(P,R)*X(P,R))) ;

*Exactly one formation must be chosen
Con1..      sum(F, y(F))            =E= 1;

*All roles in formation F must be filled
Con2(R)..   sum(F, A(F,R)*y(F))     =E= sum(P, x(P,R));

*Player P can at most fill one role
Con3(P)..   sum(R, x(P,R))          =L= 1;

*We must employ at least one quality player
Con4..      sum(R, sum(Q(P), x(P,R)))  =G= 1;

*If we employ all quality players, we ust employ at least ine strength player
Con5..      sum(R, sum(Q(P), x(P,R)))  =L= sum(R, sum(S(P), x(P,R)+card(Q)))-1;

MODEL model1 /all/;

SOLVE model1 USING mip MAXIMIZING z;

DISPLAY z.l, x.l, y.l;
























